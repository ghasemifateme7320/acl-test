const casbin = require('casbin');

const checkRole = async (userName, object, act) => {
    try {
        const enforcer = await casbin.newEnforcer('./config/model.conf','./config/policy.csv');
        return enforcer.enforce(userName, object, act)
    }catch (e) {
        console.log(e.message)
    }
};

const getRoles = async (username)=>{
    try {
        const enforcer = await casbin.newEnforcer('./config/model.conf','./config/policy.csv');
        return enforcer.getRoles(username)
    }catch (e) {
        console.log(e.message)
    }

}

module.exports ={checkRole,getRoles}