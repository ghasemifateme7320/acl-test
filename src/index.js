const casbinUtils = require('./casbinUtils');
const jwtUtils = require('./jwtUtils');


//function name must be createProduct,readProduct,deleteProduct
const isUserAuthorize = async (jwtToken, functionName)=>{
    const username = jwtUtils.verifyJwt(jwtToken).username;
    switch (functionName) {
        case 'createProduct':
            return await casbinUtils.checkRole(username,'product','create');
        case 'readProduct':
            return await casbinUtils.checkRole(username,'product','read');
        case 'deleteProduct':
            return await casbinUtils.checkRole(username,'product','delete');
        default:
            return false
    }
};


module.exports ={isUserAuthorize}