const jwt = require('jsonwebtoken');
const secret ="testSecret"
function jwtGenerator(data) {
    return jwt.sign(data, secret)
}

function verifyJwt(jwtCode) {
    return jwt.verify(jwtCode, secret)
}


module.exports = {
    jwtGenerator,
    verifyJwt,
};