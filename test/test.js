const assert = require('assert');
const casbinUtils = require('../src/casbinUtils');
const jwtUtils = require('../src/jwtUtils');
const isUserAuthorize = require('../src/index');

describe('test casbinUtils', ()=> {
    it('should return true, ali access read product ', async ()=> {
        const actual = await casbinUtils.checkRole('ali','product','read');
        assert.equal(actual,true )
    });
    it('should return true, hasan access read and create product ', async ()=> {
        const actual = await casbinUtils.checkRole('ali','product','read');
        assert.equal(actual,true )
    });
    it('should return true, mohammad access read and create and delete product ', async ()=> {
        const actual = await casbinUtils.checkRole('ali','product','read');
        assert.equal(actual,true )
    });
});


describe('test jwtUtils', ()=> {
    it('should return ali as username',()=>{
        const jwt = jwtUtils.jwtGenerator({username :"ali"});
        const actual = jwtUtils.verifyJwt(jwt);
        assert.equal(actual.username,"ali" )
    })
});

describe('test integration',  ()=> {
    it('should return true for ali read access',async()=>{
        const jwtCode = jwtUtils.jwtGenerator({username :"ali"});
        const actual = await isUserAuthorize.isUserAuthorize(jwtCode,'readProduct');
        assert.equal(actual,true)
    });
    it('should return true for ali dont have access to create product',async()=>{
        const jwtCode = jwtUtils.jwtGenerator({username :"ali"});
        const actual = await isUserAuthorize.isUserAuthorize(jwtCode,'createProduct');
        assert.equal(actual,false)
    });

    it('should return true for hasan read and write access',async()=>{
        const jwtCode = jwtUtils.jwtGenerator({username :"hasan"});
        const actual = await isUserAuthorize.isUserAuthorize(jwtCode,'readProduct');
        assert.equal(actual,true)

    });
    it('should return true for hasan create and write access',async()=>{
        const jwtCode = jwtUtils.jwtGenerator({username :"hasan"});
        const actual = await isUserAuthorize.isUserAuthorize(jwtCode,'createProduct');
        assert.equal(actual,true)
    })

});







